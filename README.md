Wanda: Agent with Non-Deterministic Actions
===========================================

*The power of control stochastic output*

Also, in reference to Wanda Marximof from Marvel universe,
the Scarlet Witch starting its career with a random power.
In some version, its power consists in manipulating chaos.

Wanda, aims to become a multi-language library to create deliberative agent based on Decision Making under Uncertainty (mainly Markov Decision Process with transition based on Bayesian Network model).
